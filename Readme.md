Compilation of tools and resources... 
because who needs bookmarks? 

## Privesc

[Windows / Linux Local Privilege Escalation Workshop](https://github.com/sagishahar/lpeworkshop)

[Linux Local Privilege Escalation Checklist](https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/)


## Windows

[SMB Share – SCF File Attacks ](https://pentestlab.blog/2017/12/13/smb-share-scf-file-attacks/)  

[Places of Interest in Stealing NetNTLM Hashes](https://osandamalith.com/2017/03/24/places-of-interest-in-stealing-netntlm-hashes/)  

[pth-toolkit](https://github.com/byt3bl33d3r/pth-toolkit)  

[Payload Download / Execution Cheat Sheet](https://arno0x0x.wordpress.com/2017/11/20/windows-oneliners-to-download-remote-payload-and-execute-arbitrary-code/)


## Linux

[Kernel RootKits. Getting your hands dirty](https://0x00sec.org/t/kernel-rootkits-getting-your-hands-dirty/1485)  

[Magic](https://asecuritysite.com/forensics/magic)  


## Android

[tsug0d/AndroidMobilePentest101](https://github.com/tsug0d/AndroidMobilePentest101)  


## OSINT

[https://start.me/p/rx6Qj8/start-page](https://start.me/p/rx6Qj8/start-page)

## Misc

[Jasmin](http://wwwi10.lrr.in.tum.de/~jasmin/downloads.html)

[Process Memory and Memory Corruptions](https://azeria-labs.com/process-memory-and-memory-corruption/)  

[samoshkin/tmux-config](https://github.com/samoshkin/tmux-config)  

[https://wargames.ret2.systems/level/corruption#](https://wargames.ret2.systems/level/corruption#)

[wmealing/LinuxKernelExploitationPlayground](https://github.com/wmealing/LinuxKernelExploitationPlayground)  

[AndroidKernelExploitationPlayground](https://github.com/Fuzion24/AndroidKernelExploitationPlayground)


